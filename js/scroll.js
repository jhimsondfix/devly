jQuery(function($){

    $(document).ready(function() {

        $(".scroll-to").click(function() {
            
            var url = "#home";           
            var myHref= $(this).attr('href');            
            var ScrollOffset = 0;

            if(myHref == url){
                var ScrollOffset = 90;
            }

            $("html, body").animate({
                
                scrollTop: $($(this).attr("href")).offset().top-ScrollOffset + "px"
           
            }, {
                duration: 1500
            });
            return false;
        });
      

    });

});
